# -*- coding: utf-8 -*-
"""
Created on Thu Jul  8 13:22:12 2021

@author: Bruno
"""
def namelist(names):
    ret =""
    if len(names)>0:
        ret =names[0]['name']
    if len(names)>1:
        for i in range(1,len(names)-1,1):
            ret =ret+ ", " + names[i]['name']

        ret =ret +" & " + names[len(names)-1]['name'] 
    return ret