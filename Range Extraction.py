# -*- coding: utf-8 -*-
"""
Created on Mon Jul 19 13:56:21 2021

@author: Bruno
"""
def solution(args):

    ret = str(args[0])
    count = 0
    for i in range(len(args)-1):
        if args[i+1]== (args[i]+1):
            count +=1
        else:
            if count > 1:
                ret = ret+'-'+str(args[i])
                count =0
            elif count == 1:
                ret = ret+','+str(args[i])
                count =0
                
            ret= ret+","+str(args[i+1])
            count =0
    if count >1:
        ret =ret+'-'+str(args[len(args)-1])
    elif count ==1:
        ret =ret+','+str(args[len(args)-1])
        
    return ret