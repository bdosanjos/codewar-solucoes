# -*- coding: utf-8 -*-
"""
Created on Mon Jul 19 15:38:41 2021

@author: Bruno
"""
def done_or_not(board):
    
    import  statistics
    ret = 'Finished!'
    for i in range(len(board)):     
        aux =[]
        if len(statistics.multimode(board[i][:])) <9:
            ret ="Try again!"
            break
        for j in range(len(board)):
            aux.append(board[j][i])
        if len(statistics.multimode(aux[:])) <9:
            ret ="Try again!"
            break
    for i in range(0,len(board),3):
        if len(statistics.multimode(board[i][0:3]+board[i+1][0:3]+board[i+2][0:3]))<9:
            ret ="Try again!"
            break
        if len(statistics.multimode(board[i][3:6]+board[i+1][3:6]+board[i+2][3:6]))<9:
            ret ="Try again!"
            break
        if len(statistics.multimode(board[i][6:]+board[i+1][6:]+board[i+2][6:]))<9:
            ret ="Try again!"
            break
    return ret