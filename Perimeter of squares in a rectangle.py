# -*- coding: utf-8 -*-
"""
Created on Tue Jul 20 22:01:33 2021

@author: Bruno
"""

#%%


def perimeter(n):
    if n<=0:
        return 0
    ret = []
    ret.append(1)
    ret.append(1)
    for i in range(2,n+1,1):
        ret.append(ret[i-1]+ret[i-2])
    return 4*sum(ret)
        