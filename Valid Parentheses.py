# -*- coding: utf-8 -*-
"""
Created on Mon Jul 19 14:02:25 2021

@author: Bruno
"""
def valid_parentheses(string):
    list = 0
    for i in string:
        if (i=="("):
            list= list + 1
        elif (i==")"):
            list= list - 1
        if list<0:
            break
    return list ==0