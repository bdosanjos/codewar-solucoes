# -*- coding: utf-8 -*-
"""
Created on Thu Jul  8 13:20:08 2021

@author: Bruno
Implement the function unique_in_order which takes as argument a sequence and returns a list of
 items without any elements with the same value
 next to each other and preserving the original order of elements.
 For example:
     
unique_in_order('AAAABBBCCDAABBB') == ['A', 'B', 'C', 'D', 'A', 'B']
unique_in_order('ABBCcAD')         == ['A', 'B', 'C', 'c', 'A', 'D']
unique_in_order([1,2,2,3,3])       == [1,2,3]
     
"""
def unique_in_order(iterable):
    ret =[]
    if iterable =="":
        return ret
    ret.append(iterable[0])
    for x in range(len(iterable)):
        if iterable[x] != ret[len(ret)-1]:
            ret.append(iterable[x])
            
    
    return ret