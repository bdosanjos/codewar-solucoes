# -*- coding: utf-8 -*-
"""
Created on Thu Jul  8 13:23:02 2021

@author: Bruno
"""
def find_even_index(arr):
    pivo =0
    for i in range(len(arr)):
        if sum(arr[:pivo])==sum(arr[pivo+1:]):
            return pivo
        pivo= pivo + 1
    return  -1