# -*- coding: utf-8 -*-
"""
Created on Thu Jul  8 13:13:03 2021

@author: Bruno


You probably know the "like" system from Facebook and other pages. People can "like" blog posts, pictures
or other items. We want to create the text that should be displayed next to such an item.

Implement a function likes :: [String] -> String, which must take in input array, containing the
names of people who like an item. It must return the display text as shown in the examples:
    
likes([]) # must be "no one likes this"
likes(["Peter"]) # must be "Peter likes this"
likes(["Jacob", "Alex"]) # must be "Jacob and Alex like this"
likes(["Max", "John", "Mark"]) # must be "Max, John and Mark like this"
likes(["Alex", "Jacob", "Mark", "Max"]) # must be "Alex, Jacob and 2 others like this"

For 4 or more names, the number in and 2 others simply increases.

"""
def likes(names):
    end = "likes this"
    ends = "like this"
    tam = len(names)
    if tam ==0:
        return "no one "+end
    elif tam == 1:
        return names[tam-1]+" "+ end 
    elif tam == 2:
        return ' and '.join(names)+" "+ends
    elif tam >= 3:
        ret = ', '.join(names[:2])
        if tam ==3:
            return ' and '.join([ret,names[tam-1]])+" "+ends
        else:
            return ' and '.join([ret,str(len(names[2:]))])+" others "+ends
        return "bola"
#