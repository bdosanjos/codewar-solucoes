# -*- coding: utf-8 -*-
"""
Created on Thu Jul  8 13:24:42 2021

@author: Bruno
"""
def alphabet_position(text):
    retorno = ""
    aux = [char for char in text]
    for i in range(len(aux)):    
        if(ord('a')<=ord(aux[i])<=ord('z')):
            retorno = retorno+str(ord(aux[i])-ord('a')+1)+" "
        if(ord('A')<=ord(aux[i])<=ord('Z')):    
            retorno = retorno+str(ord(aux[i])-ord('A')+1)+" "
    retorno = retorno[:len(retorno)-1]
    return retorno