def snail(snail_map):
    ret = []
    while len(snail_map) > 0:
        ret += snail_map[0]
        del snail_map[0]
        if len(snail_map) > 0:
            for i in snail_map:
                ret += [i[-1]]
                del i[-1]
            if snail_map[-1]:
                ret += snail_map[-1][::-1]
                del snail_map[-1]
            for i in reversed(snail_map):
                ret += [i[0]]
                del i[0]

    return ret