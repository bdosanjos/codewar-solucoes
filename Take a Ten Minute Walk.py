# -*- coding: utf-8 -*-
"""
Created on Thu Jul  8 13:21:02 2021

@author: Bruno

You live in the city of Cartesia where all roads are laid out in a perfect grid. 
You arrived ten minutes too early to an appointment, so you decided to take the 
opportunity to go for a short walk. The city provides its citizens with a Walk 
Generating App on their phones -- everytime you press the button it sends you an 
array of one-letter strings representing directions to walk (eg. ['n', 's', 'w', 'e']).
 You always walk only a single block for each letter (direction) and you know it takes
 you one minute to traverse one city block, so create a function that will 
 return true if the walk the app gives you will take you exactly ten minutes 
(you don't want to be early or late!) and will, of course, return you to your starting point. Return false otherwise.
"""
def is_valid_walk(walk):
    if len(walk)!=10:
        return False
    else:
        ns =0
        we =0
        for x in walk:
            if x=="n":
                ns = ns +1
            if x=="s":
                ns = ns -1
            if x=="w":
                we = we -1
            if x=="e":
                we = we +1
        if ns==0 and we ==0:
            return True
        else:
            return False