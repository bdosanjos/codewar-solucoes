# -*- coding: utf-8 -*-
"""
Created on Thu Jul  8 13:24:13 2021

@author: Bruno
"""
def count_bits(n):
    base = 2
    count =0
    while n >0:
        if (n % base == 1):
            count = count +1
        n = int(n/base)
        
    return count