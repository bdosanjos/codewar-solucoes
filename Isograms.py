# -*- coding: utf-8 -*-
"""
Created on Thu Jul  8 13:23:54 2021

@author: Bruno
"""
def is_isogram(string):
    dup ={}
    for char in string.lower():
        if char in dup:
            dup[char]+=1
        else:
            dup[char]=1
    return len(dup)==len(string)